﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio_de_BlackSunSamurai
{
    class EstructurasRepetitivas
    {
        public static void UnoACien()
        {
            Console.Write("Seleccione la estructura que desees utilizar: \n" +
               "1.DoWhile \n" + "2.While \n" + "3.For \n" + " \n");
            int valor = int.Parse(Console.ReadLine());
            Console.WriteLine("Suma de los 100 numeros enteros");
            switch (valor)
            {
                case 1:
                    Console.WriteLine("Estructura repetir (DoWhile)\n");
                    int a, b;
                    a = 0; b = 0;
                    do
                    {
                        a = a + 1;
                        b = b + a;
                    } while (a < 100);
                    Console.WriteLine("La suma es " + b + "\n");
                    break;
                case 2:
                    Console.WriteLine("Estructura mientras (While)");
                    int i, s;
                    i = 0; s = 0;
                    while (s < 100)
                    {
                        s = s + 1;
                        i = i + s;
                    }
                    Console.WriteLine("La suma es: " + i + "\n");
                    break;
                case 3:
                    Console.WriteLine("Estructura desde (For)");
                    int S;
                    S = 0;
                    for (int n = 1; n <= 100; n++)
                    {
                        S = S + n;
                        Console.WriteLine(S);
                    }
                    Console.WriteLine("La suma es " + S + "\n");
                    break;
            }
        }
        public static void AprovadosInformatica()
        {
            int cantidad, calificacion, a = 0;
            Console.WriteLine("Indique la cantidad de alumnos del curso \n ");
            cantidad = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingresa la calificacion de cada alumno \n");
            for (int i = 1; i < cantidad + 1; i++)
            {
                Console.WriteLine("Calificacion alumno #" + i);
                
                calificacion = int.Parse(Console.ReadLine());
                if (calificacion >= 5)
                {
                    a = a + 1;
                }
            }
            Console.WriteLine("La cantidad de aprobados es: " + a);
        }
        public static void EmpleadosEmpresa()
        {
            int cantidad, edad, a = 0;
            Console.WriteLine("Indique la cantidad de empleados \n ");
            cantidad = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese la edad de cada empleado \n");
            for (int i = 1; i < cantidad + 1; i++)
            {
                Console.WriteLine("Empleado #" + i);
                edad = int.Parse(Console.ReadLine());
                if (edad > 65)
                {
                    a = a + 1;
                }
            }
            Console.WriteLine("Existen trabajadores mayores de 65 años en un número de " + a);
        }
        public static void Empresa50()
        {
            int salario, a = 0, m = 0, b = 0;
            Console.WriteLine("Ingrese el salario de cada empleado \n");
            for (int i = 0; i < 1; i++)
            {
                Console.WriteLine("Empleado #" + (i + 1));
                salario = int.Parse(Console.ReadLine());
                {
                    if (salario > 300)
                    {
                        a = a + 1;

                    }
                    if (salario == 300)
                    {
                        m = m + 1;
                    }
                    if (salario < 300)
                    {
                        b = b + 1;
                    }
                }
            }
            Console.WriteLine("El numero de empleados con salarios altos es de " + a);
            Console.WriteLine("El numero de empleados con salarios medios es de " + m);
            Console.WriteLine("El numero de empleados con salarios bajos es de " + b);
        }
        public static void ValorMaximo()
        {
            int d, D, p;
            d = 0; D = 0;
            while (d < 999 && D<999)
            {
                Console.WriteLine("Ingrese el primer numero ");
                d = int.Parse(Console.ReadLine());
                Console.WriteLine("Ingrese el segundo numero ");
                D = int.Parse(Console.ReadLine());
                p = ((d + D) / 2);      
                Console.WriteLine("El promedio es " + p +"\n");
            }
        }
    }
}
