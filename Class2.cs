﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio_de_BlackSunSamurai
{
    class CondicionalCSharp
    {
        public static void sumaResta()
        {
            double a, b;
            Console.WriteLine("Ingresa 2 numeros");
            Console.WriteLine("\n Ingresa el primer numero");
            a = double.Parse(Console.ReadLine());
            Console.WriteLine("\n Ingresa el segundo numero");
            b = double.Parse(Console.ReadLine());
            if (a > b)
            {
                Console.WriteLine("\n La suma es " + (a + b));
                Console.WriteLine("\n La diferencia es " + (a - b));
            }
            else
            {
                Console.WriteLine("\n El producto es " + (a * b));
                Console.WriteLine("\n El cociente es " + (a / b));
            }
        }
        public static void Digitos()
        {
            decimal a;
            Console.WriteLine("Ingrese un numero positivo de 1 a 2 digitos");
            a = decimal.Parse(Console.ReadLine());
            if (a >= 10)
            {
                Console.WriteLine("Tiene 2 digitos");
            }
            else
            {
                Console.WriteLine("Tiene un digito");
            }
        }
        public static  void Sueldo()
        {
            int s;
            double b;
            Console.WriteLine("Ingresar sueldo");
            s = int.Parse(Console.ReadLine());
            if (s > 3000)
            {
                b = (s * .16);
                Console.WriteLine("Debe abonar el 16% correspondiente a su sueldo el cual es :" + b);
            }
            else
            {
                Console.WriteLine("No debe abonar ninguna cantidad");
            }
        }
        public static void ParImpar()
        {
            decimal n;
            Console.WriteLine("Ingrese un numero");
            n = decimal.Parse(Console.ReadLine());
            if (n%2==0)
            {
                Console.WriteLine("Es par");
            }
            else
            {
                Console.WriteLine("Es impar");
            }
        }
        public static void Estacionamiento()
        {
            int T;
            double p;
            Console.WriteLine("Tiempo de estacionamiento en minutos");
            T = int.Parse(Console.ReadLine());
            if (T==60)
            { Console.WriteLine("$"+6); }
            if (T>60)
            {
                p = (6 + (.15 * (T - 60)));
                Console.WriteLine("$" + p);
            }
            else
            {
                Console.WriteLine("$" + 5.50);
            }
            Console.WriteLine("Presiona una tecla para salir");
            Console.ReadKey();
        }
    }
}