﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio_de_BlackSunSamurai
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hola esta es la version corregida y mejorada de mi primer repositorio \n");
            Console.Write("Seleccione que serie de ejercicios desea revisar: \n" +
                "1.Ejercicios en clase \n" + "2.Arreglos \n" + "3.Estructuras Condicionales \n" + "4.Estructuras Repetitivas \n"+" \n");
            int valor = int.Parse(Console.ReadLine());
            switch (valor)
            {
                case 1:
                    Console.WriteLine("Ejercicios en clase");
                    Ejercicios.Usuario();
                    Ejercicios.Decimal();
                    Ejercicios.Tablas();
                    break;
                case 2:
                    Console.WriteLine("Arreglos");
                    Ejercicios.ArregloAscendente();
                    Ejercicios.ArregloDescendente();
                    break;
                case 3:
                    Console.WriteLine("Estructuras Condicionales");
                    CondicionalCSharp.sumaResta();
                    CondicionalCSharp.Digitos();
                    CondicionalCSharp.Sueldo();
                    CondicionalCSharp.ParImpar();
                    CondicionalCSharp.Estacionamiento();
                    break;
                case 4:
                    Console.WriteLine("Estructuras Repetitivas");
                    EstructurasRepetitivas.UnoACien();
                    EstructurasRepetitivas.AprovadosInformatica();
                    EstructurasRepetitivas.EmpleadosEmpresa();
                    EstructurasRepetitivas.Empresa50();
                    5
                    break;
                default:
                    Console.WriteLine("Se ingreso un valor fuera de rango");
                    break;
            }
            Console.ReadKey();
        }
    }
}